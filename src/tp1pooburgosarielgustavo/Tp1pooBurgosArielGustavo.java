
package tp1pooburgosarielgustavo;

import java.util.Scanner;

public class Tp1pooBurgosArielGustavo {

    public static void main(String[] args) {
        
        Scanner op= new Scanner(System.in);
        int opcion=0;
        int edad;
        int n1, n2, n3;
        int h=0, m = 0, s=0;
        int hora=0, minuto = 0, segundo=0;
            
            do {
                System.out.println("==MENU DE OPCIONES=="+
                               "\n1. Maximos Minimos"+
                               "\n2. Edad"+
                               "\n3. Mayor"+
                               "\n4. Segundos"+
                               "\n5. Matriz"+
                               "\n6. Salir"+
                               "\nINGRESE UNA OPCIÓN");
                
                opcion=op.nextInt();
                switch (opcion){
                    
                    case 1:
                        
                        System.out.println("Tipo\tMínimo\tMáximo");
                        System.out.println("byte\t" + Byte.MIN_VALUE + "\t" + Byte.MAX_VALUE);
                        System.out.println("short\t" + Short.MIN_VALUE + "\t" + Short.MAX_VALUE);
                        System.out.println("int\t" + Integer.MIN_VALUE + "\t" + Integer.MAX_VALUE);
                        System.out.println("long\t" + Long.MIN_VALUE + "\t" + Long.MAX_VALUE);
                        System.out.println("float\t" + Float.MIN_VALUE + "\t" + Float.MAX_VALUE);
                        System.out.println("double\t" + Double.MIN_VALUE + "\t" + Double.MAX_VALUE);
                        break;
                        
                    case 2:
                        
                        System.out.println("Ingrese una edad:");
                        edad=op.nextInt();
                        String mayor="";
                        mayor=edad>17?"Es mayor de edad":"No es mayor de edad";
                        System.out.println(mayor);
                        break;
                        
                    case 3:
                        
                        System.out.println("Ingrese 3 numeros");
                        n1=op.nextInt();
                        n2=op.nextInt();
                        n3=op.nextInt();
                        mayor=n1 > n2 && n1 > n3?n1+" es mayor":(mayor=n2 > n1 && n2 > n3?n2+" es mayor":n3+" es mayor");
                        System.out.println(mayor);
                        break;
                        
                    case 4:
                        
                        System.out.println("Ingrese Hora");
                        h=op.nextInt();
                        if (0<=h&&h<=23){
                            System.out.println("Ingrese Minutos");
                        m=op.nextInt();
                        if (0<=m&&m<=59){
                            System.out.println("Ingrese Segundos");
                        s=op.nextInt();
                        if (0<=s&&s<=59){
                            
                            if(s+1==60){
                            segundo=00;
                            minuto=m+1;
                            hora=h;
                            if (minuto==60){
                                segundo=00;
                                minuto=00;
                                hora=h+1;
                                if (hora==24){
                                    segundo=00;
                                    minuto=00;
                                    hora=00;
                                }
                            }
                        }else{segundo=s+1;minuto=m;hora=h;}
                        System.out.println("la hora ingresada es: "+h+":"+m+":"+s);
                        System.out.println("la hora sumada 1 segundo es: "+hora+":"+minuto+":"+segundo);
                        }else{System.out.println("Segundos ingresados erroneos");};
                        }else{System.out.println("Minutos ingresados erroneos");};
                        }else{System.out.println("Hora ingresada erronea");};
                        break;
                        
                    case 5:
                        int matrizA[][]= new int [4][4];
                        for (int x=0;x<matrizA.length;x++){
                           for (int y=0;y<matrizA[x].length;y++){
                               System.out.println("introduzca el elemento ["+x+","+y+"]");
                               matrizA[x][y]=op.nextInt();
                           }
                        }
                        int contador=0;
                        String matrizB[][]=new String [4][4];
                        for (int x=0;x<matrizA.length;x++){
                           for (int y=0;y<matrizA[x].length;y++){
                               matrizB[x][y]=matrizA[x][y]!=0?"Ocupado":"Vacio";
                               if (matrizB[x][y]=="Ocupado"){
                                   contador++;
                               }
                           }
                        }
                        System.out.println("Matriz A");
                           for (int x=0;x<matrizA.length;x++){
                            System.out.print("|");
                           for (int y=0;y<matrizA[x].length;y++){
                               System.out.print(matrizA[x][y]);
                               if (y!=matrizA[x].length-1) System.out.print("\t");
                           }
                           System.out.print("|\n");
                        }
                           System.out.print("|\n");
                           
                           System.out.println("Matriz B");
                        
                           for (int x=0;x<matrizB.length;x++){
                            System.out.print("|");
                           for (int y=0;y<matrizB[x].length;y++){
                               System.out.print(matrizB[x][y]);
                               if (y!=matrizB[x].length-1) System.out.print("\t");
                           }
                           System.out.print("|\n");
                        }
                           System.out.println("TOTAL ESPACIOS SIN 0: "+contador);
                        

                        break;
                    case 6:
                        System.out.println("Fin del Programa");
                        break;
                    default :
                        System.out.println("Opcion incorrecta..intente nuevamente");;
                }
            }
        
                while (opcion!=6);
        
    
        }
    
}
